package ai.wavelabs.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleController {
	
	@RequestMapping(value="/hello-sample")
	public String sayHello()
	{
		return  "Hello World";
		
	}

}
