package ai.wavelabs.demo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Columns;
import org.springframework.web.bind.annotation.RestController;
@XmlRootElement
@Entity
@Table(name="student")
public class Student {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
    @Column
	private String name;
    @Column
	private String city;
    @Column
	private String bloodgroup;
    @Column
	private int age;
	

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Student() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getBloodGrp() {
		return bloodgroup;
	}

	public void setBloodGrp(String address) {
		this.bloodgroup = bloodgroup;
	}

	public Student(int id,int age, String name, String address) {
		this.id = id;
		this.age=age;
		this.name = name;
		this.city = city;
		this.bloodgroup = bloodgroup;

	}
}
