package ai.wavelabs.demo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {

	@Autowired
	StudentService studentService;

	List<Student> list = new ArrayList<>();

	@GetMapping(value = "test")
	public String sayHello() {
		return "Test";
	}

	@GetMapping(value = "/students", produces = { MediaType.APPLICATION_JSON_VALUE, })
	public ResponseEntity<List<Student>> getStudents(@RequestParam(name = "age", required = false) Integer age) {
		List<Student> students = studentService.getAllStudents();
		return ResponseEntity.status(200).body(students);
	}

	@PostMapping(value = "students", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Student> saveStudent(@RequestBody Student student) {
		Student s = studentService.saveStudent(student);
		return ResponseEntity.status(201).body(s);
	}

	@PutMapping("students/{id}")
	public ResponseEntity<Student> updateStudent(@PathVariable("id") Integer id, @RequestBody Student student) {
		for (Student student1 : list) {
			if (student1.getId() == id) {
				list.remove(student1);
				list.add(student);
				break;
			}
		}
		return ResponseEntity.status(200).body(student);
	}

	@DeleteMapping("students/{id}")
	public ResponseEntity<Void> deleteStudent(@PathVariable("id") Integer id) {
		for (Student student1 : list) {
			if (student1.getId() == id) {
				list.remove(student1);
				break;
			}
		}
		return ResponseEntity.status(204).build();
	}
}
