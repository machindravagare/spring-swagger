package ai.wavelabs.demo;

import java.util.List;
import java.util.stream.Collectors;
import java.util.ArrayList;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication 
public class Studentacontroller {
	List<Student> list = new ArrayList<>();

	@RequestMapping(value = "/hello")
	public String sayHello() {
		return "Hello world";
	}

	@GetMapping(value="/students")
	public ResponseEntity<List<Student>> getStudents() {
		return ResponseEntity.ok().body(list);
	}
	public List<Student> getStudents(@RequestParam(name = "age", required = false) Integer age) {
		List<Student> resultList;
		if (age != null)
			resultList = list.stream().filter(s -> s.getAge() == age).collect(Collectors.toList());
		else
			resultList = list;
		
		return resultList;
	}
	
	
	@PostMapping("students")
	public ResponseEntity<Student> saveStudent(@RequestBody Student student) {
		list.add(student);
		return ResponseEntity.status(201).body(student);
	}
//
//     @PutMapping("students/{id}")
// 	public ResponseEntity<Student> updateStudent
// 	(@PathVariable("id") Integer id, @RequestBody Student student) {
// 		for (Student student1 : list) {
// 			if (student1.getId() == id) {
// 				list.remove(student1);
// 				list.add(student);
// 				break;
// 			}
// 		}
// 		return ResponseEntity.status(200).body(student);
// 	}
//     @DeleteMapping("students/{id}")
// 	public ResponseEntity<Void> deleteStudent(@PathVariable("id") Integer id) {
// 		for (Student student1 : list) {
// 			if (student1.getId() == id) {
// 				list.remove(student1);
// 				break;
// 			}
// 		}
// 		return ResponseEntity.status(204).build();
// 	}

}
